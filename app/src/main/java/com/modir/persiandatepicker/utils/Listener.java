package com.modir.persiandatepicker.utils;


import com.modir.persiandatepicker.utils.util.PersianCalendar;

/**
 * Created by aliabdolahi on 1/23/17.
 */

public interface Listener {
    void onDateSelected(PersianCalendar persianCalendar);

    void onDismissed();
}
