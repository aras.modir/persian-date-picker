package com.modir.persiandatepicker

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.modir.persiandatepicker.utils.Listener
import com.modir.persiandatepicker.utils.PersianDatePickerDialog
import com.modir.persiandatepicker.utils.util.PersianCalendar


class MainActivity : AppCompatActivity() {

    companion object {
        const val TAG = "MainActivity"
    }

    private lateinit var picker: PersianDatePickerDialog
    private lateinit var initDate: PersianCalendar
    private lateinit var button: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        button = findViewById(R.id.btn)

        setInitDate()

    }

    private fun setInitDate() {
        initDate = PersianCalendar()
        initDate.setPersianDate(1370, 1, 1)
    }

    fun openDatePicker(view: View) {
        picker = PersianDatePickerDialog(this)
                .setPositiveButtonString("باشه")
                .setNegativeButton("بیخیال")
                .setTodayButton("امروز")
                .setTodayButtonVisible(true)
                .setMinYear(1300)
                .setInitDate(initDate)
                .setMaxYear(PersianDatePickerDialog.THIS_YEAR)
                .setActionTextColor(Color.GRAY)
                .setTitleType(PersianDatePickerDialog.WEEKDAY_DAY_MONTH_YEAR)
                .setShowInBottomSheet(true)
                .setListener(object : Listener {
                    override fun onDateSelected(persianCalendar: PersianCalendar) {
                        Log.d(TAG, "onDateSelected: " + persianCalendar.gregorianChange) //Fri Oct 15 03:25:44 GMT+04:30 1582
                        Log.d(TAG, "onDateSelected: " + persianCalendar.timeInMillis) //1583253636577
                        Log.d(TAG, "onDateSelected: " + persianCalendar.time) //Tue Mar 03 20:10:36 GMT+03:30 2020
                        Log.d(TAG, "onDateSelected: " + persianCalendar.delimiter) //  /
                        Log.d(TAG, "onDateSelected: " + persianCalendar.persianLongDate) // سه‌شنبه  13  اسفند  1398
                        Log.d(TAG, "onDateSelected: " + persianCalendar.persianLongDateAndTime) //سه‌شنبه  13  اسفند  1398 ساعت 20:10:36
                        Log.d(TAG, "onDateSelected: " + persianCalendar.persianMonthName) //اسفند
                        Log.d(TAG, "onDateSelected: " + persianCalendar.isPersianLeapYear) //false
                        Toast.makeText(this@MainActivity, persianCalendar.persianYear.toString() + "/" + persianCalendar.persianMonth + "/" + persianCalendar.persianDay, Toast.LENGTH_SHORT).show()
                    }

                    override fun onDismissed() {}
                })

        picker.show()
    }

}